#!make

SOURCE_IMAGE ?= ubuntu
SOURCE_TAG ?= 20.04
TITLE = Docker Dev / Ubuntu 20.04

# used in devcontainer
-include /workspace/common/dev-setup/custom-image.mk

# used in gitlab-ci
ifeq ($(MAKE_LIB),)
include dev-setup/custom-image.mk
endif
